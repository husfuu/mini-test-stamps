//apabole program

const n = 100;
let result = "";
for (let i = 1; i <= n; i++) {
  if (i % 3 == 0 && i % 5 == 0) {
    result += "Apabole, ";
  } else if (i % 3 == 0) {
    result += "Apa, ";
  } else if (i % 5 == 0) {
    result += "Bole, ";
  } else {
    result += `${i}, `;
  }
}

console.log(result);
