const days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

const formattedDate = function (apiDate) {
  const dateObj = new Date(apiDate);
  let day = days[dateObj.getDay()];
  let date = apiDate.split("-")[2];
  let month = dateObj.toLocaleString("default", { month: "long" });
  let year = dateObj.getFullYear();

  return `${day}, ${date} ${month} ${year}`;
};

module.exports = {
  formattedDate
};
