# Menampilkan ramalan cuaca kota Jakarta untuk 5 hari kedepan

## Deskripsi

Menggunakan OpenWeather API dengan dokumentasi [ini](https://openweathermap.org/forecast5#5days).

## Cara menjalankan program

- paste API_KEY di file `.env`

```javascript
$ npm install // install dependency
$ node index // run the program
```

## Dependecy

- `axios` to fetch third party APIs
