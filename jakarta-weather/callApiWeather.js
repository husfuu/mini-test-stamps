const axios = require("axios");
require("dotenv").config();

const BASE_URL = "https://api.openweathermap.org/data/2.5/forecast";

module.exports = {
  forecastWeather: (cityName, units) =>
    axios({
      method: "GET",
      url: BASE_URL,
      params: {
        q: cityName,
        appid: process.env.API_KEY,
        units: units
      }
    })
};
