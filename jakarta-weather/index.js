const weatherAPI = require("./callApiWeather");
const { formattedDate } = require("./utils");

const cityName = "Jakarta";
const units = "metric"; // celcius

const app = async () => {
  const response = await weatherAPI.forecastWeather(cityName, units);

  const data = response.data.list;
  console.log("Weather Forecast: ");
  let nDate;
  for (let i = 0; i < data.length; i++) {
    dateAPI = data[i].dt_txt.split(" ")[0];
    if (dateAPI != nDate) {
      let dateFormatted = formattedDate(dateAPI);
      let temperature = data[i].main.temp;
      console.log(`${dateFormatted}: ${temperature}℃`);
      nDate = dateAPI;
    }
  }
};

app();
